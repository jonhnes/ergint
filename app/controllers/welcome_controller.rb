class WelcomeController < ApplicationController
  def index
  end

  def en
    I18n.locale = :en
    render :index
  end

  def pt
    I18n.locale = :pt
    render :index
  end

end
